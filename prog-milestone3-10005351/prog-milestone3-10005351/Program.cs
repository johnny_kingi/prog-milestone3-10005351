﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10005351
{
    class Program
    {
        static void Main(string[] args)
        {
            int main_loop = 1;
            do
            {
                try
                {
                    var customer = new Client();
                    var cust_pizza = new PizzaMenu();
                    cust_pizza.total_spend = 0;
                    int drink_loop = 0;
                    int drink_choice = 0;

                    Console.WriteLine("\n\t*********************************************");
                    Console.WriteLine("\t   **  Welcome to the The Pizza Place  **");
                    Console.WriteLine("\t*********************************************");
                    pizza_art();
                    Console.WriteLine("\n\t1\tEnter your details and proceed to pizza menu\n");
                    Console.WriteLine("\t2\tEnter your deails later and order pizza now\n");
                    Console.WriteLine("\tSelct option 1 or 2 and press <enter>");
                    var first_choice = int.Parse(Console.ReadLine());

                    if (first_choice == 1)
                    {
                        Console.WriteLine("\n\tPlease enter your name");
                        customer.cust_name = Console.ReadLine();
                        Console.WriteLine("\n\tNow enter your phone number");
                        customer.cust_phone = int.Parse(Console.ReadLine());
                    }
                    else
                    {
                        customer.cust_name = string.Empty;
                    }


                    cust_pizza.pizza_menu(customer);

                    Console.Clear();
                    Console.WriteLine("\n\t***************************************************************");
                    Console.WriteLine("\t\t Do you want to add Drinks to your order?");
                    Console.WriteLine("\t***************************************************************");
                    Console.WriteLine("\n\t1.\tAdd Drinks to your order\n\t2.\tProceed to payment without drinks");
                    Console.WriteLine("\n select 1 or 2 to and press <enter> to continue");
                    int drink_menu = int.Parse(Console.ReadLine());

                    if (drink_menu == 1)
                    {
                        cust_pizza.drinks_menu(customer, drink_choice, drink_loop);
                    }
                    {
                        //else do nothing
                    }


                name_check: //label

                    if (customer.cust_name == string.Empty)
                    {
                        Console.WriteLine("\n\tYou must enter your details to proceed to payment");
                        Console.WriteLine("\n\tType in your name and press <enter>");
                        customer.cust_name = Console.ReadLine();
                        Console.WriteLine($"\n\tThanks {customer.cust_name} now type in your phone number and press <enter>");
                        customer.cust_phone = int.Parse(Console.ReadLine());

                        goto name_check;
                    }
                    {
                        //else do nothing
                    }

                    Console.Clear();

                    total_calc(customer, cust_pizza);

                    Console.Clear();
                    Console.WriteLine("\n\t*********************************");
                    Console.WriteLine("\n\t*  Thanks for using Pizza Menu  *");
                    Console.WriteLine("\n\t*********************************");
                    Console.WriteLine($"\n\tPizzas wont be long -- Thanks {customer.cust_name} and ENJOY!!");
                    Console.WriteLine("\n\n\t -- Press any key to exit --");
                    Console.ReadKey();
                    main_loop = 2;
                }
                catch
                {
                    Console.WriteLine("\t****  Something went wrong ****\n\t** make sure you are using the correct entries for menu choices **\n");
                    Console.WriteLine("\t -- press any key to start again --");
                    Console.ReadKey();
                    Console.Clear();
                    main_loop = 1;
                }
            } while (main_loop == 1);
        }
            public static void pizza_art()
        {
            Console.WriteLine("\t        d8b                         ");
            Console.WriteLine("\t        Y8P");
            Console.WriteLine("\t");
            Console.WriteLine("\t88888b. 8888888888888888888 8888b.");
            Console.WriteLine("\t888  88b888   d88P    d88P     88b");
            Console.WriteLine("\t888  888888  d88P    d88P.d888888");
            Console.WriteLine("\t888 d88P888 d88P    d88P  888  888");
            Console.WriteLine("\t88888P  8888888888888888888Y888888");
            Console.WriteLine("\t888");
            Console.WriteLine("\t888");
            Console.WriteLine("\t888");
        }

        //==================================================================================================

        public static void total_calc(Client customer, PizzaMenu cust_pizza)    //Method for calculating change to give
        {
            double pay_amt = 0.00;
            double change = 0.00;
            double total = 0.00;

            Console.WriteLine($"\tYour Order: {customer.cust_name}\n");
            Console.WriteLine("\t********************************\n");
            foreach (var x in customer.order)
            {
                Console.WriteLine($"\t{x.Key} -- ${x.Value}\n");
            }
            Console.WriteLine("\t********************************\n");
            foreach (var x in customer.order.Values)
            {
                 total = total + x;
            }
            Console.WriteLine("\t================================\n");
            Console.WriteLine($"\tYour total is ${total}\n");
            Console.WriteLine("\t================================\n");
            Console.WriteLine("\n\tPlease enter amount paid:  ");
            pay_amt = double.Parse(Console.ReadLine());

           change = pay_amt - total;
           
            if(pay_amt < total)
            {
                Console.WriteLine($"More money please, you still owe {change}");
                Console.WriteLine("\n\tPlease confirm balance paid by press <enter>  ");
                Console.ReadLine();
                change = 0;
            }
            {
                //else do nothing
            }           

            Console.WriteLine($"\n\tThanks {customer.cust_name} your change is ${change}");
            Console.WriteLine("\n\t -- Press any key to continue --");
            Console.ReadKey();
            
        }
    }

    //==================================================================================================
    public class Client // class 1. - Client  -
    {
        public Dictionary<string, double> order;
        public string cust_name;
        public int cust_phone;

        public Client()  // constructor for client
        {
            Dictionary<string, double> order = new Dictionary<string, double>();
            cust_name = "Joe Bloggs";
            cust_phone = 0;
        }

    }

    //===================================================================================================
    public class PizzaMenu  // class2. - Pizza  & Drinks Menu -
    {
        public static string mlv;
        public static string hwn;
        public static string glc;
        public static string cc;
        public static string bch;
        public static string size;
        public static string s;
        public static string m;
        public static string l;
        public static string menu_opt;
        public double total_spend;

        public void pizza_menu(Client customer)     //Method for pizza menu
        {
            mlv = "Meatlovers";
            hwn = "Hawaiian";
            glc = "Garlic";
            cc = "Cheesy Cheese";
            bch = "Bacon & Chicken";
            size = "\n\t1.\tLarge\n \t2.\tSmall\n\n\t-- Choose 1 or 2 and press <enter> --";
            s = "small";
            m = "Medium";
            l = "Large";
            menu_opt = "choose 1 to order another pizza or 2 if you've finished choosing your pizza";
            total_spend = 0;

            customer.order = new Dictionary<string, double>();

            int control = 0;
            do
            {
                Console.Clear();
                int pizza_choice = 0;
                Console.WriteLine("\t*******************************************************************");
                Console.WriteLine("\t\t\t\t --  PIZZA MENU  --");
                Console.WriteLine("\t*******************************************************************\n");
                Console.WriteLine("\tPlease choose the number of the pizza you would like then press <enter>");
                Console.WriteLine("\tThen select what size you want and press <enter>\n");
                Console.WriteLine($"\n\t1.\t{mlv}\t {s} $10.00 / {l} $14.00");
                Console.WriteLine($"\n\t2.\t{hwn}\t {s} $8.00 / {l} $12.00");
                Console.WriteLine($"\n\t3.\t{glc}\t\t {s} $8.00 / {l} $12.00");
                Console.WriteLine($"\n\t4.\t{cc}\t {s} $6.00 / {l} $10.00");
                Console.WriteLine($"\n\t5.\t{bch}\t {s} $10.00 / {l} $14.00");
                Console.WriteLine($"\n\t6.\tView your current selection");
                Console.WriteLine("\n\t7.\tExit Pizza Menu");

                pizza_choice = int.Parse(Console.ReadLine());

                switch (pizza_choice)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine($"\n\tYou chose {mlv}! -- Now choose what size:");
                        Console.WriteLine($"\t{size}");
                        var large_choice = int.Parse(Console.ReadLine());

                        if (large_choice == 1)
                        {
                            customer.order.Add($"{mlv} {l}", 14.00);
                        }
                        else if (large_choice == 2)
                        {
                            customer.order.Add($"{mlv} {s}", 10.00);
                        }
                        {
                            Console.WriteLine($"\n\t{menu_opt}");
                            control = int.Parse(Console.ReadLine());
                        }
                        break;

                    case 2:
                        Console.Clear();
                        Console.WriteLine($"\n\tYou chose {hwn} -- Now choose what size:");
                        Console.WriteLine($"\t{size}");
                        var large_choice2 = int.Parse(Console.ReadLine());

                        if (large_choice2 == 1)
                        {
                            customer.order.Add($"{hwn} {l}", 12.00);
                        }
                        else if (large_choice2 == 2)
                        {
                            customer.order.Add($"{hwn} {s}", 8.00);
                        }
                        {
                            Console.WriteLine($"\n\t{menu_opt}");
                            control = int.Parse(Console.ReadLine());
                        }
                        break;

                    case 3:
                        Console.Clear();
                        Console.WriteLine($"\n\tYou chose {glc} -- Now choose what size:");
                        Console.WriteLine($"\n\t{size}");
                        var large_choice3 = int.Parse(Console.ReadLine());

                        if (large_choice3 == 1)
                        {
                            customer.order.Add($"{glc} {l}", 12.00);
                        }
                        else if (large_choice3 == 2)
                        {
                            customer.order.Add($"{glc} {s}", 8.00);
                        }
                        {
                            Console.WriteLine($"\n\t{menu_opt}");
                            control = int.Parse(Console.ReadLine());
                        }
                        break;

                    case 4:
                        Console.Clear();
                        Console.WriteLine($"\n\tYou chose {cc}! -- Now choose what size:");
                        Console.WriteLine($"\t{size}");
                        var large_choice4 = int.Parse(Console.ReadLine());

                        if (large_choice4 == 1)
                        {
                            customer.order.Add($"{cc} {l}", 10.00);
                        }
                        else if (large_choice4 == 2)
                        {
                            customer.order.Add($"{cc} {s}", 6.00);
                        }
                        {
                            Console.WriteLine($"\n\t{menu_opt}");
                            control = int.Parse(Console.ReadLine());
                        }
                        break;

                    case 5:
                        Console.Clear();
                        Console.WriteLine($"\n\tYou chose {bch}! -- Now choose what size:");
                        Console.WriteLine($"\t{size}");
                        var large_choice5 = int.Parse(Console.ReadLine());

                        if (large_choice5 == 1)
                        {
                            customer.order.Add($"{bch} {l}", 14.00);
                        }
                        else if (large_choice5 == 2)
                        {
                            customer.order.Add($"{bch} {s}", 10.00);
                        }
                        {
                            Console.WriteLine($"\n\t{menu_opt}");
                            control = int.Parse(Console.ReadLine());
                        }
                        break;

                    case 6:
                        Console.Clear();
                        Console.WriteLine($"\n\tYour order so far:\n");
                        double quote = 0;

                        foreach (var x in customer.order)
                        {
                            Console.WriteLine($"\t{x.Key}  --  ${x.Value}\n");
                        }

                        foreach (var x in customer.order.Values)
                        {
                            quote = quote + x;
                        }

                        Console.WriteLine($"\n\tYour total spend so far is ${quote}");
                        Console.WriteLine("\n\n\t-- Press any key to return to the main menu --");
                        Console.ReadKey();
                        break;

                    case 7:
                        {
                            control = 7;
                        }
                        break;

                    default:
                        break;

                }
            } while (control == 1);

            Console.Clear();
            Console.WriteLine($"\n\tGreat Choices {customer.cust_name}!!\n");

            foreach (var x in customer.order)
            {
                Console.WriteLine($"\t{x.Key}\t\t${x.Value}.00\n");
            }

            foreach (var x in customer.order.Values)
            {
                total_spend = total_spend + x;
            }

            Console.WriteLine("\n\t********************************************");
            Console.WriteLine($"\n\t   Your total spend so far is ${total_spend}.00");
            Console.WriteLine("\n\t********************************************");
            Console.WriteLine("\n\n\t --Press any key to continue to continue--");
            Console.ReadKey();
        }

        //===================================================================================================

        public void drinks_menu(Client customer,int drink_choice,int drink_loop)    //method for drink menu
        {
            drink_loop = 1;
            do
            {
                drink_choice = 0;
                string coke = "CocaCola";
                string fanta = "Fanta";
                string lnp = "L & P";
                string juice = "Orange Juice";
                string beer = "Cold Beer";

                Console.Clear();
                Console.WriteLine("\n\t*******************************************************************");
                Console.WriteLine("\t*******************  Welcome to the Drink menu  *******************");
                Console.WriteLine("\t*****************************************************************\n");
                Console.WriteLine("\tPlease choose the number of the Drink you would like then press <enter>\n");

                Console.WriteLine($"\t1.\t{coke}, $4.50");
                Console.WriteLine($"\t2.\t{fanta}, $4.50");
                Console.WriteLine($"\t3.\t{lnp}, $4.50");
                Console.WriteLine($"\t4.\t{juice}, $5.00");
                Console.WriteLine($"\t5.\t{beer}, $7.00");
                Console.WriteLine($"\t6.\tContinue to payment");

                drink_choice = int.Parse(Console.ReadLine());
                
                switch (drink_choice)
                {
                    case 1:
                        Console.WriteLine($"\n\t{coke} -- $4.50 added to your order");
                        customer.order.Add(coke, 4.50);

                        Console.WriteLine("\n\t1.\tChoose another drink");
                        Console.WriteLine("\n\t2.\tProceed to payment menu");
                        drink_loop = int.Parse(Console.ReadLine());
                        break;

                    case 2:
                        Console.WriteLine($"\n\t{fanta} -- $4.50 added to your order");
                        customer.order.Add(fanta, 4.50);

                        Console.WriteLine("\n\t1.\tChoose another drink");
                        Console.WriteLine("\n\t2.\tProceed to payment menu");
                        drink_loop = int.Parse(Console.ReadLine());
                        break;

                    case 3:
                        Console.WriteLine($"\n\t{lnp} -- $4.50 added to your order");
                        customer.order.Add(lnp, 4.50);

                        Console.WriteLine("\n\t1.\tChoose another drink");
                        Console.WriteLine("\n\t2.\tProceed to payment menu");
                        drink_loop = int.Parse(Console.ReadLine());
                        break;
                    case 4:
                        Console.WriteLine($"\n\t{juice} -- $4.50 added to your order");
                        customer.order.Add(juice, 5.00);

                        Console.WriteLine("\n\t1.\tChoose another drink");
                        Console.WriteLine("\n\t2.\tProceed to payment menu");
                        drink_loop = int.Parse(Console.ReadLine());
                        break;

                    case 5:
                        Console.WriteLine($"\n\t{beer} -- $7.00 added to your order");
                        customer.order.Add(beer, 7.00);

                        Console.WriteLine("\n\t1.\tChoose another drink");
                        Console.WriteLine("\n\t2.\tProceed to payment menu");
                        drink_loop = int.Parse(Console.ReadLine());

                        break;

                    case 6:
                        drink_loop = 2;
                        break;

                    default:
                        break;   
                } 
                    
            } while (drink_loop == 1);
        }
    }
}

       

        
    

